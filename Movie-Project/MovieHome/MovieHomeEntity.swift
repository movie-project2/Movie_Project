//
//  MovieHomeEntity.swift
//  Movie-Project
//
//  Created by Go Christian Goszal on 08/03/23.
//

import Foundation

struct MovieHomeEntity: Codable {
    let results: [Movie]
}

struct Movie: Codable {
    let id: Int
    let title: String?
    let poster_path: String?
}
