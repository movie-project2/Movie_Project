//
//  MovieHomeViewController.swift
//  Movie-Project
//
//  Created by Go Christian Goszal on 08/03/23.
//

import UIKit

enum Sections: Int {
    case Trending = 0
    case Popular = 1
    case Upcoming = 2
    case Action = 3
    case Comedy = 4
}

class MovieHomeViewController: UITabBarController {

    private var randomTrendingMovie: Movie?
    private var headerView: HeaderUIView?
    var movieList: Array<Movie> = Array()
    var presenter: (ViewToPresenterMovieHomeProtocol & InteractorToPresenterMovieHomeProtocol)?
    
    let sectionTitles: [String] = ["Trending", "Popular", "Upcoming", "Action", "Comedy"]
    
    private let tableView: UITableView = {
        let table = UITableView(frame: .zero, style: .grouped)
        table.register(CollectionViewTableViewCell.self, forCellReuseIdentifier: CollectionViewTableViewCell.identifier)
        return table
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        view.addSubview(tableView)
        tableView.delegate = self
        tableView.dataSource = self
        
        headerView = HeaderUIView(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height: 500))
        tableView.tableHeaderView = headerView
        configureHeaderView()
    }
    
    private func configureHeaderView() {

        presenter.shared.getTrendingMovies { [weak self] result in
            switch result {
            case .success(let titles):
                let selectedMovie = titles.randomElement()
                
                self?.randomTrendingMovie = selectedMovie
                self?.headerView?.configure(with: MovieViewModel(titleName: selectedMovie?.title ?? "", posterURL: selectedMovie?.poster_path ?? ""))
                
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = view.bounds
    }

}

extension MovieHomeViewController:PresenterToViewMovieHomeProtocol{
    
    func showMovie(movieArray: Array<MovieHomeEntity>) {

        self.movieArrayList = movieArray
        self.tableView.reloadData()
//        hideProgressIndicator(view: self.view)
        
    }
    
    func showError() {
        let alert = UIAlertController(title: "Alert", message: "Problem Fetching Notice", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
}

extension MovieHomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionTitles.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CollectionViewTableViewCell.identifier, for: indexPath) as? CollectionViewTableViewCell else {
            return UITableViewCell()
        }
        
        cell.delegate = self

        switch indexPath.section {
        case Sections.Trending.rawValue:
            presenter?.shared.getTrendingMovies { result in
                switch result {
                    
                case .success(let movies):
                    cell.configure(with: movies)
                case .failure(let error):
                    print(error)
                }
            }
            
        case Sections.Popular.rawValue:
            presenter?.shared.getPopularMovies { result in
                switch result {
                case .success(let movies):
                    cell.configure(with: movies)
                case .failure(let error):
                    print(error)
                }
            }
            
        case Sections.Upcoming.rawValue:
            presenter?.shared.getUpcomingMovies { result in
                switch result {
                case .success(let movies):
                    cell.configure(with: movies)
                case .failure(let error):
                    print(error)
                }
            }
            
        case Sections.Action.rawValue:
            presenter?.shared.getActionMovies { result in
                switch result {
                case .success(let movies):
                    cell.configure(with: movies)
                case .failure(let error):
                    print(error)
                }
            }
            
        case Sections.Comedy.rawValue:
            presenter?.shared.getComedyMovies { result in
                switch result {
                case .success(let movies):
                    cell.configure(with: movies)
                case .failure(let error):
                    print(error)
                }
            }
        default:
            return UITableViewCell()

        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 280
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else {return}
        header.textLabel?.font = .systemFont(ofSize: 18, weight: .semibold)
        header.textLabel?.frame = CGRect(x: header.bounds.origin.x + 20, y: header.bounds.origin.y, width: 100, height: header.bounds.height)
        header.textLabel?.textColor = .white
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionTitles[section]
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let defaultOffset = view.safeAreaInsets.top
        let offset = scrollView.contentOffset.y + defaultOffset
        
        navigationController?.navigationBar.transform = .init(translationX: 0, y: min(0, -offset))
    }
}
