//
//  MovieHomePresenter.swift
//  Movie-Project
//
//  Created by Go Christian Goszal on 09/03/23.
//

import Foundation

class MovieHomePresenter:ViewToPresenterMovieHomeProtocol {
    
    var view: PresenterToViewMovieHomeProtocol?
    
    var interactor: PresenterToInteractorMovieHomeProtocol?
    
    var router: PresenterToRouterMovieHomeProtocol?
    
    func getTrendingMovies(movieList: [Movie]) {
        interactor?.getTrendingMovies { result in
            switch result {
            case .success(let movies):
                movieList = movies
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getPopularMovies(movieList: [Movie]) {
        interactor?.getTrendingMovies { result in
            switch result {
            case .success(let movies):
                movieList = movies
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getUpcomingMovies(movieList: [Movie]) {
        interactor?.getUpcomingMovies { result in
            switch result {
            case .success(let movies):
                movieList = movies
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getActionMovies(movieList: [Movie]) {
        interactor?.getActionMovies { result in
            switch result {
            case .success(let movies):
                movieList = movies
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getComedyMovies(movieList: [Movie]) {
        interactor?.getComedyMovies { result in
            switch result {
            case .success(let movies):
                movieList = movies
            case .failure(let error):
                print(error)
            }
        }
    }
    
}

extension MovieHomePresenter: InteractorToPresenterMovieHomeProtocol{
    
    func movieFetchedSuccess(movieArray: Array<MovieHomeEntity>) {
        view?.showMovie(movieArray: movieArray)
    }
    
    func movieFetchFailed() {
        view?.showError()
    }
    
    
}
