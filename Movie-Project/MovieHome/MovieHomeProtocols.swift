//
//  MovieHomeProtocols.swift
//  Movie-Project
//
//  Created by Go Christian Goszal on 08/03/23.
//

import Foundation
import UIKit

protocol ViewToPresenterMovieHomeProtocol{
    
    var view: PresenterToViewMovieHomeProtocol? {get set}
    var interactor: PresenterToInteractorMovieHomeProtocol? {get set}
    var router: PresenterToRouterMovieHomeProtocol? {get set}
    func getTrendingMovies(movieList: [Movie])
    func getPopularMovies(movieList: [Movie])
    func getUpcomingMovies(movieList: [Movie])
    func getActionMovies(movieList: [Movie])
    func getComedyMovies(movieList: [Movie])

}

protocol PresenterToViewMovieHomeProtocol{
    func showMovie(movieArray:Array<MovieHomeEntity>)
    func showError()
}

protocol PresenterToRouterMovieHomeProtocol{
    static func createModule()-> MovieHomeViewController
}

protocol PresenterToInteractorMovieHomeProtocol{
    var presenter:InteractorToPresenterMovieHomeProtocol? {get set}
    func getTrendingMovies(completion: @escaping (Result<[Movie], Error>) -> Void)
    func getPopularMovies(completion: @escaping (Result<[Movie], Error>) -> Void)
    func getUpcomingMovies(completion: @escaping (Result<[Movie], Error>) -> Void)
    func getActionMovies(completion: @escaping (Result<[Movie], Error>) -> Void)
    func getComedyMovies(completion: @escaping (Result<[Movie], Error>) -> Void)
}

protocol InteractorToPresenterMovieHomeProtocol{
    func movieFetchedSuccess(movieArray:Array<MovieHomeEntity>)
    func movieFetchFailed()
}
