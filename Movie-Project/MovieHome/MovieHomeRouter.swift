//
//  MovieHomeRouter.swift
//  Movie-Project
//
//  Created by Go Christian Goszal on 09/03/23.
//

import Foundation
import UIKit

class MovieHomeRouter:PresenterToRouterMovieHomeProtocol{
    
    static func createModule() -> MovieHomeViewController {
        
        let view = mainstoryboard.instantiateViewController(withIdentifier: "MovieHomeViewController") as! MovieHomeViewController
        
        var presenter: ViewToPresenterMovieHomeProtocol & InteractorToPresenterMovieHomeProtocol = MovieHomePresenter()
        var interactor: PresenterToInteractorMovieHomeProtocol = MovieHomeInteractor()
        var router:PresenterToRouterMovieHomeProtocol = MovieHomeRouter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        return view
        
    }
    
    static var mainstoryboard: UIStoryboard{
        return UIStoryboard(name:"Main",bundle: Bundle.main)
    }
    
    func pushToMovieScreen(navigationConroller navigationController:UINavigationController) {
        
        let movieModule = MovieHomeRouter.createModule()
        navigationController.pushViewController(movieModule,animated: true)
        
    }
    
}
